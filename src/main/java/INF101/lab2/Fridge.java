package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {
    int max_size;
    List<FridgeItem> items_fridge;

    public Fridge(){
        max_size = 20;
        items_fridge = new ArrayList<FridgeItem>();

    }

    public static void main(String[] args){
    }

    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return items_fridge.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items_fridge.size() < max_size){
            items_fridge.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items_fridge.contains(item)) {
            items_fridge.remove(item);
        } else {
            throw new NoSuchElementException ();
        }
    }

    @Override
    public void emptyFridge() {
        items_fridge.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired_items = new ArrayList<FridgeItem>();

        for (FridgeItem item : items_fridge){
            if (item.hasExpired()){
                expired_items.add(item);
            }
        }
        items_fridge.removeAll(expired_items);

        return expired_items;
    }


}
